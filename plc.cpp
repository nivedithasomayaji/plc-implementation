namespace {
  // Hello2 - The second implementation with getAnalysisUsage implemented.
  	struct Hello2 : public FunctionPass {
    	static char ID; // Pass identification, replacement for typeid
    	Hello2() : FunctionPass(ID) {}

    	bool runOnFunction(Function &F) override {
      		++HelloCounter;
    	  	errs() << "Hello: ";
      		errs().write_escaped(F.getName()) << '\n';
      		//PLC

      		//create global var
      		Module *mod = F.getParent();
      		IRBuilder<> b1(mod->getContext());
      		GlobalVariable *gVarCount = createGlob(builder, "count");
      		//global array
      		ArrayType* ArrayTy_0 = ArrayType::get(IntegerType::get(mod->getContext(), 32), 10);
      		GlobalVariable* gvar_array_a = new GlobalVariable(*mod,ArrayTy_0,false,GlobalValue::ExternalLinkage,0,"a");
      		gvar_array_a->setAlignment(16);
      		ConstantAggregateZero* const_array_2 = ConstantAggregateZero::get(ArrayTy_0);
			gvar_array_a->setInitializer(const_array_2);
      		std::vector<Instruction *> ins;
      		for (BasicBlock &BB1 : F) {
      			for(Instruction &I : BB1) {
      				ins.push_back(I);
      			} 
      		}

      		//Declare Pthread functions

			Type* argsPTC[] = {pthreadTy->getPointerTo(), vt, plc->getFunctionType()->getPointerTo(), vt};
			FunctionType* pthreadCreateTy = FunctionType::get(Type::getInt32Ty(M.getContext()), ArrayRef<Type*>(argsPTC,4), false);
			Constant *pthread_create = M.getOrInsertFunction("pthread_create", pthreadCreateTy);
			Type *argsPTC1[] = {Type::getInt64Ty(M.getContext()),vt1};
			FunctionType *pthread_joinTy=FunctionType::get(Type::getInt32Ty(M.getContext()),ArrayRef<Type*>(argsPTC1,2), false);
			Constant *pthread_join = M.getOrInsertFunction("pthread_join", pthread_joinTy);
      		
      		//creating function for creating and calling threads
      		FunctionType *funcType = llvm::FunctionType::get(b1.getIntPtrTy(),false);
			Function *CFunct = llvm::Function::Create(funcType, llvm::Function::ExternalLinkage, "pThreadFunc", mod);
      		verifyFunction(*CFunct);
      		BasicBlock *BB = BasicBlock::Create(mod->getContext(),"Label1",CFunct);
      		IRBuilder<> b2(BB);
      		b2.CreateCall(CFunct->getParent()->getFunction("pthread_create"));
      		b2.CreateCall(CFunct->getParent()->getFunction("pthread_join"));

      		//find nodes with pointer deferencing
      		for(Instruction &I : ins) {
      			if(LoadInst *l = dyn_cast<LoadInst>(I)){
      				if(l->isDeferenceable()){
      					BasicBlock *split = l->getParent()->splitBasicBlock(l->getNextNode());
      					TerminatorInst *T = l->getParent()->getTerminator();
      					IRBuilder<> build(l->getParent());
      					build.CreateCall(F.getParent()->getFunction("pThreadFunc"));
      					T->removeFromParent();
      						
      				}
      			}
      		}
      		

      		return false;
    	}

	    // We don't modify the program, so we preserve all analyses.
    	void getAnalysisUsage(AnalysisUsage &AU) const override {
      		AU.setPreservesAll();
    	}
  	};
}

char Hello2::ID = 0;
static RegisterPass<Hello2>
Y("hello2", "Hello World Pass (with getAnalysisUsage implemented)");

